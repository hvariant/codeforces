import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * Created by spss on 31/10/16.
 */
public class Problem50A {
    public static class MyScanner{
        BufferedReader br;
        StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException{
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException{
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) throws IOException{
        int m,n;
        MyScanner scan = new MyScanner();

        m = scan.nextInt();
        n = scan.nextInt();

        if(m == 1){
            System.out.println(n/2);
        } else if(m % 2 == 0){
            System.out.println(m/2*n);
        } else if(n % 2 == 0){
            System.out.println(n/2*m);
        } else {
            System.out.println(Integer.max((n-1)/2*m + m/2, (m-1)/2*n + n/2));
        }
    }
}
