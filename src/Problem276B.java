/**
 * Created by spss on 16-12-5.
 */

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Problem276B {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static boolean winPosition(int totalChars, Map<Character,Integer> charCounts){
        int oddChars = (int)charCounts.entrySet().stream().filter(kv -> kv.getValue() > 0 && kv.getValue() % 2 == 1).count();

        if(totalChars % 2 == 0){
            return oddChars == 0;
        } else {
            return oddChars == 1;
        }
    }

    public static void makeMove(int totalChars, Map<Character,Integer> charCounts){
        //backup option
        char firstChar = charCounts.keySet()
                            .stream()
                            .filter(k -> charCounts.get(k) > 0)
                            .findFirst()
                            .orElse('-'); // this should NEVER happen

        List<Character> charCountsKeys = charCounts.keySet().stream().filter(k -> charCounts.get(k) > 0).collect(Collectors.toList());
        for(char c : charCountsKeys){
            charCounts.put(c, charCounts.get(c)-1); //try removing this char
            if(!winPosition(totalChars-1,charCounts)){
                return;
            }
            charCounts.put(c, charCounts.get(c)+1); //doesn't work
        }

        //impossible to win...
        charCounts.put(firstChar, charCounts.get(firstChar)-1);
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        String s = scan.next();

        Map<Character,Integer> charCounts = new HashMap<>();
        for(char c : s.chars().mapToObj(i -> (char)i).collect(Collectors.toList())){
            charCounts.put(c, charCounts.getOrDefault(c,0)+1);
        }

        int totalChars = s.length();
        boolean first = true;
        while(!winPosition(totalChars,charCounts)){
            makeMove(totalChars,charCounts);
            totalChars--;
            first = !first;
        }

        if(first){
            out.println("First");
        } else {
            out.println("Second");
        }

        out.close();
    }
}