/**
 * Created by spss on 16-12-5.
 */

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Problem445A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static void fill(int root, Set<Integer> vs, Map<Integer,Character> assignment, Map<Integer, ArrayList<Integer>> adjacentPoints){
        LinkedList<Integer> Q = new LinkedList<>();

        Q.add(root);
        vs.add(root);
        assignment.put(root,'B');

        while(Q.size() > 0){
            int cur = Q.removeFirst();
            Character curColor = assignment.get(cur);
            Character nextColor = curColor == 'B' ? 'W' : 'B';

            ArrayList<Integer> adjs = adjacentPoints.get(cur);
            for(int nc : adjs){
                if(!vs.contains(nc)){
                    Q.add(nc);
                    vs.add(nc);
                    assignment.put(nc,nextColor);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n, m;
        n = scan.nextInt();
        m = scan.nextInt();

        char[][] state = new char[n][m];
        for (int i = 0; i < n; i++) {
            String s = scan.next();
            for (int j = 0; j < m; j++) {
                state[i][j] = s.charAt(j);
            }
        }

        //global DS
        ArrayList<Integer> goodXY = new ArrayList<>();
        Map<Integer, ArrayList<Integer>> adjacentPoints = new HashMap<>();

        //fill graph structure
        for (int i = 0; i < n; i++){
            for (int j = 0; j < m; j++){
                if (state[i][j] == '.') {
                    int c = i * m + j;

                    //nodes
                    goodXY.add(c);

                    //edges
                    int[][] deltas = new int[][]{{0, -1}, {-1, 0}, {1, 0}, {0, 1}};
                    ArrayList<Integer> adjs = new ArrayList<>();
                    for (int k = 0; k < deltas.length; k++) {
                        int di = deltas[k][0];
                        int dj = deltas[k][1];

                        int ni = i + di;
                        int nj = j + dj;
                        if (0 <= ni && ni < n && 0 <= nj && nj < m && state[ni][nj] == '.') {
                            int nc = ni * m + nj;
                            adjs.add(nc);
                        }
                    }
                    adjacentPoints.put(c, adjs);
                }
            }
        }

        //fill isolated islands using bfs
        Map<Integer, Character> assignments = new HashMap<>();
        Set<Integer> vs = new HashSet<>();
        for(int c : goodXY)
            if(!vs.contains(c))
                fill(c,vs,assignments,adjacentPoints);

        //fill the actual map
        for(int c : assignments.keySet()){
            int i = c/m;
            int j = c%m;

            state[i][j] = assignments.get(c);
        }

        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                System.out.print(state[i][j]);
            }
            System.out.println();
        }
    }
}