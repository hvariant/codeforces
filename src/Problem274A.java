/**
 * Created by spss on 16-12-7.
 */

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

public class Problem274A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int n;
        int k;
        ArrayList<Integer> a = new ArrayList<>();

        n = scan.nextInt();
        k = scan.nextInt();
        for(int i=0;i<n;i++) a.add(scan.nextInt());
        Collections.sort(a);

        Set<Integer> S = new HashSet<>();
        for(int i=0;i<n;i++){
            int ai = a.get(i);
            if(ai % k != 0 || !S.contains(ai/k)) S.add(ai);
        }

        out.println(S.size());
        out.close();
    }
}