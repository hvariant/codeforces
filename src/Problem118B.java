/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Problem118B {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static void printLine(int i,int n){
        System.out.print(Stream.generate(() -> new Character(' ')).limit(2*(n-i)).map(c -> c.toString()).collect(Collectors.joining()));

        ArrayList<Integer> numbers = new ArrayList<>();
        IntStream.range(0,i).forEach(k -> numbers.add(k));
        IntStream.range(0,i+1).forEach(k -> numbers.add(i-k));
        System.out.println(numbers.stream().map(k -> k.toString()).collect(Collectors.joining(" ")));
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n = scan.nextInt();

        for(int i=0;i<n;i++){
            printLine(i,n);
        }
        printLine(n,n);
        for(int i=n-1;i>=0;i--){
            printLine(i,n);
        }
    }
}