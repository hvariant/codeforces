/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Problem230A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int s,n;
        s = scan.nextInt();
        n = scan.nextInt();

        Set<Integer> dragons = IntStream.range(0,n).boxed().collect(Collectors.toSet());

        ArrayList<Integer> x = new ArrayList<>();
        ArrayList<Integer> y = new ArrayList<>();
        for(int i=0;i<n;i++){
            x.add(scan.nextInt());
            y.add(scan.nextInt());
        }

        while(dragons.size() > 0){
            final int s_final = s;
            int i = dragons.stream().filter(k -> x.get(k) < s_final).max((k1,k2) -> Integer.compare(y.get(k1),y.get(k2))).orElse(-1);

            if(i < 0) break;

            dragons.remove(i);
            s += y.get(i);
        }

        System.out.println(dragons.size() > 0 ? "NO" : "YES");
    }
}