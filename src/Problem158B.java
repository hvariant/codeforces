import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * Created by spss on 31/10/16.
 */
public class Problem158B {
    public static class MyScanner{
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException{
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) throws IOException{
        MyScanner scan = new MyScanner();
        int n = scan.nextInt();
        int[] count = new int[]{0,0,0,0,0};

        for(int i=0;i<n;i++){
            count[scan.nextInt()]++;
        }

        int R = 0;
        boolean children_left = count[1] != 0 || count[2] != 0 || count[3] != 0 || count[4] != 0;
        while(children_left){
            int room_left = 4;
            while(room_left > 0){
                boolean can_place = false;
                for(int i=4;i>=1;i--){
                    if(room_left >= i && count[i] > 0){
                        count[i]--;
                        room_left -= i;
                        can_place = true;

                        break;
                    }
                }

                if(!can_place) break;
            }

            R++;
            children_left = count[1] != 0 || count[2] != 0 || count[3] != 0 || count[4] != 0;
        }

        System.out.println(R);
    }
}
