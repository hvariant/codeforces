/**
 * Created by spss on 16-12-7.
 */

import java.io.*;
import java.util.*;

public class Problem215A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    private static class MyPair{
        int ai,bj;

        MyPair(int ai,int bj){
            this.ai = ai;
            this.bj = bj;
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int n = scan.nextInt();
        ArrayList<Integer> a = new ArrayList<>();
        for(int i=0;i<n;i++) a.add(scan.nextInt());

        int m = scan.nextInt();
        ArrayList<Integer> b = new ArrayList<>();
        for(int i=0;i<m;i++) b.add(scan.nextInt());

        ArrayList<MyPair> myPairs = new ArrayList<>();
        a.stream().forEach(ai -> b.stream().forEach(bi -> myPairs.add(new MyPair(ai,bi))));

        MyPair bestPair = myPairs.stream().filter(p -> p.bj % p.ai == 0).max((p1,p2) -> Integer.compare(p1.bj/p1.ai,p2.bj/p2.ai)).get();
        int bestCount = (int)myPairs.stream().filter(p -> p.bj % p.ai == 0).filter(p -> p.bj / p.ai == bestPair.bj / bestPair.ai).count();
        out.println(bestCount);
        out.close();
    }
}