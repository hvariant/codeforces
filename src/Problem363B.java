/**
 * Created by spss on 16-12-7.
 */

import java.io.*;
import java.util.*;
import java.util.stream.IntStream;

public class Problem363B {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int n,k;
        ArrayList<Integer> h = new ArrayList<>();
        ArrayList<Integer> S = new ArrayList<>();

        n = scan.nextInt();
        k = scan.nextInt();

        S.add(0);
        for(int i=0;i<n;i++)
            S.add(S.get(S.size()-1) + scan.nextInt());

        int j = IntStream.range(1,n-k+2).boxed()
                    .min((i1,i2) -> Integer.compare(S.get(i1+k-1)-S.get(i1-1), S.get(i2+k-1)-S.get(i2-1))).orElse(1);

        out.println(j);
        out.close();
    }
}