/**
 * Created by spss on 16-12-6.
 */

import java.io.*;
import java.util.*;

public class Problem450A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    private static class Child{
        int a,index;

        Child(int a,int index){
            this.a = a;
            this.index = index;
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int n,m;
        LinkedList<Child> children = new LinkedList<>();

        n = scan.nextInt();
        m = scan.nextInt();
        for(int i=0;i<n;i++){
            int ai = scan.nextInt();
            children.add(new Child(ai,i+1));
        }

        while(children.size() > 1){
            Child front = children.removeFirst();
            front.a -= m;
            if(front.a > 0){
                children.addLast(front);
            }
        }

        out.println(children.getFirst().index);
        out.close();
    }
}