/**
 * Created by spss on 16-12-4.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class Problem32B {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        String code = scan.next();
        ArrayList<Integer> digits = new ArrayList<>();

        int i=0;
        while(i < code.length()){
            if(code.charAt(i) == '.'){
                digits.add(0);
                i++;
            } else {
                if(code.charAt(i+1) == '.'){
                    digits.add(1);
                    i += 2;
                } else {
                    digits.add(2);
                    i += 2;
                }
            }
        }

        System.out.println(digits.stream().map(n -> n.toString()).collect(Collectors.joining()));
    }
}