/**
 * Created by spss on 16-12-4.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class Problem271A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static boolean distinctDigits(int year){
        String yearStr = Integer.toString(year);
        Set<Integer> digits = yearStr.chars().boxed().collect(Collectors.toSet());
        return digits.size() == 4;
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int year = scan.nextInt();
        do{ year++; } while(!distinctDigits(year));

        System.out.println(year);
    }
}