/**
 * Created by spss on 16-12-4.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Problem266B {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static void moveQueue(ArrayList<Character> queue){
        int i=0;

        while(i < queue.size() - 1){
            if(queue.get(i) == 'B' && queue.get(i+1) == 'G'){
                queue.set(i,'G');
                queue.set(i+1,'B');

                i += 2;
            } else {
                i++;
            }
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n,t;
        ArrayList<Character> queue = new ArrayList<>();

        n = scan.nextInt();
        t = scan.nextInt();
        String q = scan.next();
        q.chars().forEach(c -> queue.add((char)c));

        IntStream.range(0,t).forEach(_i -> moveQueue(queue));

        System.out.println(queue.stream().map(c -> Character.toString(c)).collect(Collectors.joining()));
    }
}