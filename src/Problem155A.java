/**
 * Created by spss on 27/11/16.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Problem155A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n = scan.nextInt();
        int maxPts, minPts;
        int cool = 0;

        maxPts = minPts = scan.nextInt();
        for(int i=1;i<n;i++){
            int pts = scan.nextInt();

            if(pts < minPts){
                minPts = pts;
                cool++;
            } else if(pts > maxPts){
                maxPts = pts;
                cool++;
            }
        }

        System.out.println(cool);
    }
}