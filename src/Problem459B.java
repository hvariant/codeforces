/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Problem459B {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n;
        ArrayList<Integer> b = new ArrayList<>();

        n = scan.nextInt();
        for(int i=0;i<n;i++){
            b.add(scan.nextInt());
        }

        int maxB = b.stream().max(Integer::compare).get();
        int minB = b.stream().min(Integer::compare).get();
        long maxCount = b.stream().filter(_n -> _n == maxB).count();
        long minCount = b.stream().filter(_n -> _n == minB).count();

        if(maxB != minB) {
            System.out.println((maxB - minB) + " " + (maxCount * minCount));
        } else {
            System.out.println(0 + " " + maxCount*(maxCount-1)/2);
        }
    }
}