/**
 * Created by spss on 16-12-7.
 */

import java.io.*;
import java.util.*;

public class Problem350A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int minAi=Integer.MAX_VALUE,maxAi=Integer.MIN_VALUE;
        int minBj=Integer.MAX_VALUE;

        int n,m;
        n = scan.nextInt();
        m = scan.nextInt();
        for(int i=0;i<n;i++){
            int ai = scan.nextInt();
            minAi = Integer.min(minAi,ai);
            maxAi = Integer.max(maxAi,ai);
        }
        for(int j=0;j<m;j++){
            int bj = scan.nextInt();
            minBj = Integer.min(minBj,bj);
        }

        int lowerBound = Integer.max(2*minAi,maxAi);
        int upperBound = minBj;

        out.println(lowerBound >= upperBound ? -1 : lowerBound);
        out.close();
    }
}