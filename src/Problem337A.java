/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Problem337A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n,m;
        n = scan.nextInt();
        m = scan.nextInt();
        ArrayList<Integer> f = new ArrayList<>();
        for(int i=0;i<m;i++) f.add(scan.nextInt());

        Collections.sort(f);
        int minDiff = Integer.MAX_VALUE;
        for(int i=0;i<m-n+1;i++){
            if(f.get(i+n-1) - f.get(i) < minDiff){
                minDiff = f.get(i+n-1) - f.get(i);
            }
        }

        //System.out.println(f.stream().map(i -> i.toString()).collect(Collectors.joining(" ")));
        System.out.println(minDiff);
    }
}