/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.StringTokenizer;

public class Problem144A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n = scan.nextInt();
        ArrayList<Integer> heights = new ArrayList<>();
        for(int i=0;i<n;i++){
            heights.add(scan.nextInt());
        }

        int maxHeight = Collections.max(heights);
        int minHeight = Collections.min(heights);

        if(maxHeight == minHeight){
            System.out.println(0);
            return;
        }

        int i = 0;
        while(heights.get(i) != maxHeight) i++;
        int j = heights.size()-1;
        while(heights.get(j) != minHeight) j--;

        if(i < j){
            System.out.println(i + (heights.size()-j-1));
        } else {
            System.out.println(i + (heights.size()-j-1)-1);
        }
    }
}