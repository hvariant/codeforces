import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by spss on 1/11/16.
 */
public class Problem462B {
    public static class MyScanner{
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException{
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) throws IOException{
        MyScanner scan = new MyScanner();
        int n,k;
        String letters;

        n = scan.nextInt();
        k = scan.nextInt();
        letters = scan.next();

        Map<Character, Long> charMap = new HashMap<>();
        for(int i=0;i<letters.length();i++){
            char c = letters.charAt(i);

            charMap.put(c, charMap.getOrDefault(c,0L) + 1);
        }

        ArrayList<Long> X = new ArrayList<>(charMap.values());
        Collections.sort(X,(n1,n2) -> Long.compare(n2,n1));

        BigInteger S = new BigInteger("0");
        long K = k;
        for(int i=0;i<X.size();i++){
            long x = X.get(i);

            if(x >= K){
                S = S.add(new BigInteger(Long.toString(K*K)));
                break;
            } else {
                K -= x;
                S = S.add(new BigInteger(Long.toString(x*x)));
            }
        }

        System.out.println(S);
    }
}
