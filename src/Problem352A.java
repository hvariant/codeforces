/**
 * Created by spss on 16-12-6.
 */

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Problem352A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int n = scan.nextInt();
        int fives=0,zeros=0;
        for(int i=0;i<n;i++){
            int c = scan.nextInt();
            if(c == 0) zeros++;
            if(c == 5) fives++;
        }

        if(zeros == 0){
            out.println(-1);
        } else {
            if (fives >= 9) {
                int k5 = fives / 9;
                out.println(IntStream.range(0, k5 * 9).mapToObj(_i -> "5").collect(Collectors.joining()) + IntStream.range(0, zeros).mapToObj(_i -> "0").collect(Collectors.joining()));
            } else {
                out.println(0);
            }
        }

        out.close();
    }
}