/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Problem141A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        Map<Character,Integer> nameChars = new HashMap<>();
        Map<Character,Integer> pileChars = new HashMap<>();

        String name = scan.next();
        String host = scan.next();
        String pile = scan.next();

        name.chars().forEach(c -> nameChars.put((char)c, nameChars.getOrDefault((char)c,0)+1));
        host.chars().forEach(c -> nameChars.put((char)c, nameChars.getOrDefault((char)c,0)+1));
        pile.chars().forEach(c -> pileChars.put((char)c, pileChars.getOrDefault((char)c,0)+1));
//
//        System.out.println(name + host);
//        System.out.println(nameChars.get('P'));
//        System.out.println(pile);
//        System.out.println(pileChars.get('P'));

        System.out.println(
                nameChars.keySet().stream().allMatch(k -> nameChars.getOrDefault(k,0) == pileChars.getOrDefault(k,0)) &&
                pileChars.keySet().stream().allMatch(k -> nameChars.getOrDefault(k,0) == pileChars.getOrDefault(k,0))
                ? "YES" : "NO");
    }
}