/**
 * Created by spss on 27/11/16.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Problem248A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n = scan.nextInt();
        int l0,l1,r0,r1;

        l0 = l1 = r0 = r1 = 0;
        for(int i=0;i<n;i++){
            int a,b;
            a = scan.nextInt();
            b = scan.nextInt();

            if(a > 0) l1++;
            else l0++;
            if(b > 0) r1++;
            else r0++;
        }

        //System.out.println("l0 = " + l0 + ", l1 = " + l1 + ", r0 = " + r0 + ", r1 = " + r1);

        int r = Arrays.stream(new int[]{l0 + r0, l0 + r1, l1 + r0, l1 + r1}).min().orElse(0);
        System.out.println(r);
    }
}