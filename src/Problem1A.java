import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by spss on 8/16/16.
 */
public class Problem1A {
    public static void main(String[] args) {
        BigInteger n,m,a;
        Scanner scan = new Scanner(System.in);

        n = scan.nextBigInteger();
        m = scan.nextBigInteger();
        a = scan.nextBigInteger();

        BigInteger w = n.add(a).add(BigInteger.valueOf(-1)).divide(a);
        BigInteger h = m.add(a).add(BigInteger.valueOf(-1)).divide(a);

        System.out.println(w.multiply(h));
    }
}
