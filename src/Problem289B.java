import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

/**
 * Created by spss on 1/11/16.
 */
public class Problem289B {
    public static class MyScanner{
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException{
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) throws IOException{
        int n,m,d,N;
        MyScanner scan = new MyScanner();

        n = scan.nextInt();
        m = scan.nextInt();
        d = scan.nextInt();
        N = n*m;

        ArrayList<Integer> A = new ArrayList<>();
        for(int i=0;i<N;i++){
            int t = scan.nextInt();
            A.add(t);
        }
        Collections.sort(A);
        int minA = A.get(0);
        int maxA = A.get(A.size()-1);

        //check the obvious
        if(maxA == minA){
            System.out.println(0);
            return;
        }

        //check for possibility
        if( (maxA - minA) % d != 0 ){
            System.out.println(-1);
            return;
        }
        boolean impossible = false;
        for(int i=0;i<N-1;i++){
            int ai = A.get(i);
            int ai_1 = A.get(i+1);

            if((ai_1 - ai) % d != 0){
                impossible = true;
                break;
            }
        }
        if(impossible){
            System.out.println(-1);
            return;
        }

        //now we can go on computing stuff
        ArrayList<Integer> S = new ArrayList<>();
        for(int k=0;k<N-1;k++){
            int s = 0;
            for(int i=0;i<=k;i++){
                s += A.get(k) - A.get(i);
            }
            for(int i=k+1;i<N;i++){
                s += A.get(i) - A.get(k+1);
            }

            S.add(s);
        }

        //System.out.print("S:");
        //for(int i=0;i<S.size();i++) System.out.print(S.get(i) + " ");
        //System.out.println();

        int minC = Integer.MAX_VALUE;
        int mu = A.get(0);
        int k = 0;
        do{
            //System.out.println("mu = " + mu);
            //System.out.println("k = " + k);

            int C = (k+1)*(mu - A.get(k)) + (N-k-1)*(A.get(k+1) - mu) + S.get(k);
            if(C < minC) minC = C;

            mu += d;
            if(mu > A.get(A.size()-1)) break;

            while(k < N-1 && A.get(k+1) < mu) k++;
        } while(mu <= A.get(A.size()-1) && k < N-1);

        System.out.println(minC/d);
    }
}
