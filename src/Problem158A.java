import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

/**
 * Created by spss on 31/10/16.
 */
public class Problem158A {
    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() throws IOException{
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }
            return st.nextToken();
        }

        int nextInt() throws IOException{
            return Integer.parseInt(next());
        }

        String nextLine() throws IOException{
            return br.readLine();
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        int n,k;
        ArrayList<Integer> scores = new ArrayList<>();

        n = scan.nextInt();
        k = scan.nextInt();

        for(int i=0;i<n;i++){
            int s = scan.nextInt();
            scores.add(s);
        }

        Collections.sort(scores,(i1,i2) -> Integer.compare(i2,i1));

        int threshold = scores.get(k-1);
        int count = 0;
        for(int i=0;i<n;i++){
            if(scores.get(i) <= 0) break;

            if(scores.get(i) >= threshold)
                count++;
        }

        System.out.println(count);
    }
}
