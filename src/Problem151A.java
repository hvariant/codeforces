/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;

public class Problem151A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n,k,l,c,d,p,nl,np;
        n = scan.nextInt();
        k = scan.nextInt();
        l = scan.nextInt();
        c = scan.nextInt();
        d = scan.nextInt();
        p = scan.nextInt();
        nl = scan.nextInt();
        np = scan.nextInt();

        int numDrinks = k*l / nl;
        int numLimeSlices = c*d;
        int numSalt = p / np;

        int maxToast = Collections.min(Arrays.asList(new Integer[]{numDrinks, numLimeSlices, numSalt}));
        System.out.println(maxToast/n);
    }
}