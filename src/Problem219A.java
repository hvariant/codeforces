/**
 * Created by spss on 16-12-7.
 */

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem219A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int k = scan.nextInt();
        String s = scan.next();
        Map<Character, Integer> charCounts = new HashMap<>();
        s.chars().forEach(c -> charCounts.put((char)c,charCounts.getOrDefault((char)c,0)+1));

        if(!charCounts.values().stream().allMatch(v -> v % k == 0)){
            out.println(-1);
        } else {
            ArrayList<Character> chars = new ArrayList<>();
            charCounts.entrySet().stream()
                    .forEach(e ->
                            chars.addAll(
                                    Stream.generate(() ->
                                            e.getKey()).limit(e.getValue()/k).collect(Collectors.toList())
                            )
                    );

            String pat = chars.stream().map(c -> c.toString()).collect(Collectors.joining());
            for(int i=0;i<k;i++){
                out.print(pat);
            }
        }

        out.close();
    }
}