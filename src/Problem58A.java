/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class Problem58A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        String s = scan.next();
        String pat = "hello";

        boolean success = true;
        int cursor = 0;
        for(int i=0;i<pat.length();i++){
            String c = pat.substring(i,i+1);
            int next = s.indexOf(c,cursor);

            if(next < 0){ //fail!
                success = false;
                break;
            } else {
                cursor = next+1;
            }
        }

        System.out.println(success ? "YES" : "NO");
    }
}