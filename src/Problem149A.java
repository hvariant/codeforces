/**
 * Created by spss on 16-12-7.
 */

import java.io.*;
import java.time.Month;
import java.util.*;

public class Problem149A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int k = scan.nextInt();
        ArrayList<Integer> a = new ArrayList<>();
        for(int i=0;i<12;i++) a.add(scan.nextInt());

        Collections.sort(a,(i1,i2) -> -Integer.compare(i1,i2));
        int i=0;
        int sum=0;
        while(i < 12 && sum < k){
            sum += a.get(i);
            i++;
        }

        if(sum < k){
            out.println(-1);
        } else {
            out.println(i);
        }

        out.close();
    }
}