/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

public class Problem80A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    static boolean isPrime(int n){
        if(n <= 1) return false;

        int L = (int)Math.sqrt(n);
        return IntStream.range(2,L+1).allMatch(k -> n % k != 0);
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n,m;
        n = scan.nextInt();
        m = scan.nextInt();

        if(!isPrime(m)){
            System.out.println("NO");
        } else {
            boolean yes = true;
            for(int k=n+1;k<m;k++){
                if(isPrime(k)){
                    yes = false;
                    break;
                }
            }

            System.out.println(yes ? "YES" : "NO");
        }
    }
}