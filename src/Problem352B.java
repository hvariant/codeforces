/**
 * Created by spss on 16-12-5.
 */

import java.io.*;
import java.util.*;

public class Problem352B {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object...objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object...objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    static class Result{
        int x,px;

        Result(int x,int px){
            this.x = x;
            this.px = px;
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int n;
        ArrayList<Integer> a = new ArrayList<>();
        Map<Integer, Integer> valCurrentIndex = new TreeMap<>();
        Map<Integer, Integer> valPx = new TreeMap<>();

        n = scan.nextInt();
        for(int i=0;i<n;i++){
            int ai = scan.nextInt();
            a.add(ai);

            if(!valCurrentIndex.containsKey(ai)){ //first time seen
                valCurrentIndex.put(ai,i);
                valPx.put(ai,0);
            } else if(valPx.get(ai) == 0){ //second time seen
                valPx.put(ai, i - valCurrentIndex.get(ai));
                valCurrentIndex.put(ai,i);
            } else if(valPx.get(ai) > 0){ //still valid Px?
                if(i - valCurrentIndex.get(ai) == valPx.get(ai)){ //keep progressing
                    valCurrentIndex.put(ai,i);
                } else {
                    valPx.put(ai,-1); //invalid progression
                }
            }
        }

        ArrayList<Result> results = new ArrayList<>();
        valPx.entrySet().stream().filter(kv -> kv.getValue() >= 0).forEach(kv -> results.add(new Result(kv.getKey(),kv.getValue())));
        Collections.sort(results,(r1,r2) -> Integer.compare(r1.x,r2.x));

        out.println(results.size());
        for(Result res : results){
            out.println(res.x + " " + res.px);
        }
        out.close();
    }
}