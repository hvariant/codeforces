/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Problem275A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int[][] presses = new int[3][3];
        boolean[][] light = new boolean[3][3];

        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                light[i][j] = true;
                presses[i][j] = scan.nextInt();
            }
        }

        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(presses[i][j] % 2 != 0){
//                    System.out.println("===================");
//
//                    System.out.println("i=" + i + ",j=" + j);

                    for(int k=Math.max(0,j-1);k<=Math.min(2,j+1);k++) light[i][k] = !light[i][k];
                    for(int k=Math.max(0,i-1);k<=Math.min(2,i+1);k++) light[k][j] = !light[k][j];
                    light[i][j] = !light[i][j];
//
//                    for(int l=0;l<3;l++){
//                        final int row = l;
//                        System.out.println(IntStream.range(0,3).mapToObj(n -> light[row][n] ? "1" : "0").collect(Collectors.joining()));
//                    }
//
//                    System.out.println("===================");
                }
            }
        }

        for(int i=0;i<3;i++){
            final int row = i;
            System.out.println(IntStream.range(0,3).mapToObj(n -> light[row][n] ? "1" : "0").collect(Collectors.joining()));
        }
    }
}