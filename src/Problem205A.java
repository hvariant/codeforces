/**
 * Created by spss on 16-12-6.
 */

import java.io.*;
import java.util.*;
import java.util.stream.IntStream;

public class Problem205A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int n = scan.nextInt();
        ArrayList<Integer> distants = new ArrayList<>();
        for(int i=0;i<n;i++) distants.add(scan.nextInt());

        int minIndex = IntStream.range(0,n).boxed().min((i1,i2) -> Integer.compare(distants.get(i1),distants.get(i2))).get();
        int minDis = distants.get(minIndex);
        int minCount = (int)distants.stream().filter(i -> i == minDis).count();

        if(minCount > 1){
            out.println("Still Rozdil");
        } else {
            out.println(minIndex+1);
        }

        out.close();
    }
}