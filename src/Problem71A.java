import java.util.Scanner;

/**
 * Created by spss on 31/10/16.
 */
public class Problem71A {
    static String shorten(String s){
        if(s.length() <= 10) return s;

        return String.format("%c%d%c", s.charAt(0), s.length()-2, s.charAt(s.length()-1));
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        scan.nextLine();
        for(int i=0;i<n;i++){
            String s = scan.nextLine();

            System.out.println(shorten(s));
        }
    }
}
