/**
 * Created by spss on 16-12-6.
 */

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Problem431B {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    private static void findAllPermutations(int cur, LinkedList<Integer> L, ArrayList<ArrayList<Integer>> perms){
        if(cur == 5){
            perms.add(new ArrayList<>(L));
            return;
        }

        for(int i=0;i<5;i++){
            final int i_final = i;
            boolean vs = L.stream().anyMatch(k -> i_final == k);

            if(!vs){
                L.addLast(i);
                findAllPermutations(cur+1,L,perms);
                L.removeLast();
            }
        }
    }

    private static int simulateQueue(ArrayList<Integer> perm, int[][] g){
        LinkedList<Integer> queue = new LinkedList<>(perm);

        int ret = 0;
        while(queue.size() > 0){
            Iterator<Integer> it = queue.iterator();
            while(it.hasNext()){
                int u,v;

                u = it.next();
                if(!it.hasNext()) break;
                v = it.next();

                ret += g[u][v] + g[v][u];
            }

            queue.removeFirst();
        }

        return ret;
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int[][] g = new int[5][5];
        for(int i=0;i<5;i++){
            for(int j=0;j<5;j++){
                g[i][j] = scan.nextInt();
            }
        }

        ArrayList<ArrayList<Integer>> perms = new ArrayList<>();
        findAllPermutations(0,new LinkedList<>(),perms);
        int maxHappiness = perms.stream().map(p -> simulateQueue(p,g)).max(Integer::compare).get().intValue();

        out.println(maxHappiness);
        out.close();
    }
}