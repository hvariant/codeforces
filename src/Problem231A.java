import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * Created by spss on 31/10/16.
 */
public class Problem231A {
    public static class MyScanner{
        BufferedReader br;
        StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException{
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) throws IOException{
        int n,count;
        MyScanner scanner = new MyScanner();

        count = 0;
        n = scanner.nextInt();
        for(int i=0;i<n;i++){
            int x,y,z;

            x = scanner.nextInt();
            y = scanner.nextInt();
            z = scanner.nextInt();

            if(x + y + z >= 2){
                count++;
            }
        }

        System.out.println(count);
    }
}
