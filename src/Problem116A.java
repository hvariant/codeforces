/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

public class Problem116A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n = scan.nextInt();
        int passengers = 0;
        int capacity = 0;
        for(int i=0;i<n;i++){
            int ai,bi;

            ai = scan.nextInt();
            bi = scan.nextInt();

            passengers -= ai;
            passengers += bi;
            capacity = Math.max(capacity,passengers);
        }

        System.out.println(capacity);
    }
}