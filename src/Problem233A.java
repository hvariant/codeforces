/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Problem233A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n = scan.nextInt();
        if(n % 2 != 0){
            System.out.println(-1);
        } else {
            System.out.println(
                    IntStream.range(0,n/2)
                             .mapToObj( i -> Arrays.asList(new Integer[]{i * 2 + 2, i * 2 + 1} ))
                             .flatMap(Collection::stream)
                             .map(i -> i.toString())
                             .collect(Collectors.joining(" "))
            );
        }
    }
}