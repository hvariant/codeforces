/**
 * Created by spss on 16-12-6.
 */

import java.io.*;
import java.util.*;

public class Problem320A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        String number = scan.next();

        boolean R = true;
        int i=0;
        while(i < number.length()){
            if(number.charAt(i) != '1'){
                R = false;
                break;
            }

            boolean success = false;

            //'144'
            if(i+2 < number.length()){
                success = number.substring(i,i+3).equals("144");
            }
            if(success){
                i+=3;
                continue;
            }

            //'14'
            if(i+1 < number.length()){
                success = number.substring(i,i+2).equals("14");
            }
            if(success){
                i+=2;
                continue;
            }

            //'1'
            success = number.substring(i,i+1).equals("1");
            if(!success){
                R = false;
                break;
            }
            i++;
        }

        out.println(R ? "YES" : "NO");
        out.close();
    }
}