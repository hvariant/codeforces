import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by spss on 31/10/16.
 */
public class Problem118A {
    public static class MyScanner{
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException{
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }
    }
    public static void main(String[] args) throws IOException{
        MyScanner scan = new MyScanner();
        String word = scan.next();
        Set<Character> vowels = new HashSet<>();
        "aeiouAEIOUyY".chars().forEach(i -> vowels.add((char)i));

        ArrayList<String> parts = new ArrayList<>();
        word.chars().forEach((int c) -> {
            if(vowels.contains((char)c)) return;

            parts.add(String.format(".%c", Character.toLowerCase(c)));
        });

        System.out.println(parts.stream().collect(Collectors.joining()));
    }
}
