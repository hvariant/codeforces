/**
 * Created by spss on 16-12-7.
 */

import java.io.*;
import java.util.*;

public class Problem251A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }

        long nextLong() throws IOException {
            return Long.parseLong(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static int findBoundary(int startFrom,long d,ArrayList<Long> X){
        int i,j;
        i = startFrom+1;
        j = X.size()-1;

        if(X.get(i) - X.get(startFrom) > d) return startFrom;
        if(X.get(j) - X.get(startFrom) <= d) return j;

        while(j-i > 1){
            int mid = (i+j)/2;

            //X[mid] is too large
            if(X.get(mid) - X.get(startFrom) > d) j = mid-1;
            else i = mid;
        }

        while(i < X.size() && X.get(i) - X.get(startFrom) <= d) i++;
        return --i;
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        ArrayList<Long> X = new ArrayList<>();
        long n,d;
        n = scan.nextLong();
        d = scan.nextLong();
        for(int i=0;i<n;i++) X.add(scan.nextLong());

        long r = 0;
        for(int i=0;i<n-2;i++){
            //j = argmax(X[j] - X[i]) where X[j] - X[i] <= d
            //      j
            // i.e. X[j] <= X[i] + d

            int j = findBoundary(i,d,X);
            if(j >= i+2){
                r += ((long)(j-i))*((long)(j-i-1))/2;
            }
        }

        out.println(r);
        out.close();
    }
}