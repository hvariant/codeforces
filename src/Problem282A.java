import java.util.Scanner;

/**
 * Created by spss on 8/16/16.
 */
public class Problem282A {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = Integer.parseInt(scan.nextLine());
        int x = 0;

        for(int i=0;i<n;i++){
            String s = scan.nextLine();
            if(s.charAt(1) == '+'){
                x++;
            } else {
                x--;
            }
        }

        System.out.println(x);
    }
}
