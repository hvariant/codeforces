/**
 * Created by spss on 27/11/16.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Problem148A {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int k,l,m,n,d;

        k = scan.nextInt();
        l = scan.nextInt();
        m = scan.nextInt();
        n = scan.nextInt();
        d = scan.nextInt();

        ArrayList<Boolean> damaged = new ArrayList<>(
                Stream.generate(() -> new Boolean(false)).limit(d).collect(Collectors.toList())
        );

        for(int i=1;i<=d/k;i++){
            damaged.set(i*k-1,true);
        }
        for(int i=1;i<=d/l;i++){
            damaged.set(i*l-1,true);
        }
        for(int i=1;i<=d/m;i++){
            damaged.set(i*m-1,true);
        }
        for(int i=1;i<=d/n;i++){
            damaged.set(i*n-1,true);
        }

        System.out.println(damaged.stream().filter(b -> b).count());
    }
}