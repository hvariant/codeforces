import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by spss on 1/11/16.
 */
public class Problem315A {
    public static class MyScanner{
        BufferedReader br;
        StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException{
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) throws IOException{
        int n;
        int[] a,b;
        MyScanner scan = new MyScanner();

        n = scan.nextInt();
        a = new int[n];
        b = new int[n];

        boolean[] opened = new boolean[n];
        for(int i=0;i<n;i++) opened[i] = false;

        for(int i=0;i<n;i++){
            a[i] = scan.nextInt();
            b[i] = scan.nextInt();
        }

        ArrayList<HashSet<Integer>> edges = new ArrayList<>();
        for(int i=0;i<n;i++){
            HashSet<Integer> hs = new HashSet<>();

            for(int j=0;j<n;j++){
                if(i==j) continue;
                if(b[i] == a[j]) hs.add(j);
            }

            edges.add(hs);
        }
        for(int i=0;i<n;i++){
            for(int j : edges.get(i)){
                opened[j] = true;
            }
        }

        int count = 0;
        for(int i=0;i<n;i++)
            if(opened[i]) count++;

        System.out.println(n - count);
    }
}
