import java.util.Scanner;

/**
 * Created by spss on 31/10/16.
 */
public class Problem4A {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int w = scan.nextInt();

        boolean possible = false;
        for(int i=2;i<w;i+=2){
            if((w-i) % 2 == 0){
                possible = true;
                break;
            }
        }

        if(possible) System.out.println("YES");
        else System.out.printf("NO");
    }
}
