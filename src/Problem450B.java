import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * Created by spss on 1/11/16.
 */
public class Problem450B {
    public static class MyScanner{
        BufferedReader br;
        StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        long nextLong() throws IOException{
            return Long.parseLong(next());
        }
    }

    public static void main(String[] args) throws IOException{
        MyScanner scan = new MyScanner();
        long x,y,n;
        long N = 1000000007;

        x = scan.nextLong();
        y = scan.nextLong();
        n = scan.nextLong();

        long[] cycle = {x,y,y-x,-x,-y,x-y};
        long fn = cycle[(int)(n-1)%6];

        if(fn > 0){
            System.out.println(fn % N);
        } else {
            long r = fn;
            while(r < 0) r += N;
            System.out.println(r);
        }
    }
}
