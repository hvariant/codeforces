import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * Created by spss on 1/11/16.
 */
public class Problem69A {
    public static class MyScanner{
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }
    }

    public static void main(String[] args) throws IOException{
        int n,x_bar,y_bar,z_bar;
        MyScanner scan = new MyScanner();

        x_bar = y_bar = z_bar = 0;
        n = Integer.parseInt(scan.next());
        for(int i=0;i<n;i++){
            int x,y,z;
            x = Integer.parseInt(scan.next());
            y = Integer.parseInt(scan.next());
            z = Integer.parseInt(scan.next());

            x_bar += x;
            y_bar += y;
            z_bar += z;
        }

        if(x_bar == y_bar && y_bar == z_bar && z_bar == 0) System.out.println("YES");
        else System.out.println("NO");
    }
}
