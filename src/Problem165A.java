/**
 * Created by spss on 16-12-6.
 */

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Problem165A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    private enum Direction{
        UPPER,LOWER,LEFT,RIGHT,NULL;
    }

    private static class Point{
        int x,y;

        Point(int x,int y){
            this.x = x;
            this.y = y;
        }
    }

    private static Direction getDirection(Point o, Point p){
        if(o.x < p.x && o.y == p.y){ //right
            return Direction.RIGHT;
        } else if (o.x > p.x && o.y == p.y){ //left
            return Direction.LEFT;
        } else if (o.x == p.x && o.y > p.y){ //lower
            return Direction.LOWER;
        } else if (o.x == p.x && o.y < p.y){ //upper
            return Direction.UPPER;
        }

        return Direction.NULL;
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int n;
        ArrayList<Point> points = new ArrayList<>();

        n = scan.nextInt();
        for(int i=0;i<n;i++){
            int x,y;
            x = scan.nextInt();
            y = scan.nextInt();
            points.add(new Point(x,y));
        }

        int superCentralPoints =
                (int)points.stream().filter(o -> {
                    return points.stream().map(p -> getDirection(o, p))
                                   .filter(dir -> dir != Direction.NULL)
                                   .collect(Collectors.toSet()).size() == 4;
                }).count();

        out.println(superCentralPoints);
        out.close();
    }
}