/**
 * Created by spss on 16-12-6.
 */

import java.io.*;
import java.util.*;

public class Problem298B {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    private static String getDir(int x1,int y1,int x2,int y2){
        String ret = "";

        if(y1 > y2){
            ret += "S";
        } else if(y1 < y2){
            ret += "N";
        }
        if(x1 > x2){
            ret += "W";
        } else if(x1 < x2){
            ret += "E";
        }

        return ret;
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int t,sx,sy,ex,ey;
        String winds;
        t = scan.nextInt();
        sx = scan.nextInt();
        sy = scan.nextInt();
        ex = scan.nextInt();
        ey = scan.nextInt();
        winds = scan.next();

        int curx = sx;
        int cury = sy;
        int curt = 0;
        while(curt < t){
            if(curx == ex && cury == ey) break;

            String dirs = getDir(curx,cury,ex,ey);
            char windDir = winds.charAt(curt);

            if(dirs.indexOf(windDir) >= 0){ //windDir helps boat reach dest
                switch(windDir){
                    case 'E':
                    {
                        curx++;
                        break;
                    }
                    case 'S':
                    {
                        cury--;
                        break;
                    }
                    case 'W':
                    {
                        curx--;
                        break;
                    }
                    case 'N':
                    {
                        cury++;
                        break;
                    }
                }
            } else {
                //anchor
            }

            curt++;
        }

        if(curx == ex && cury == ey){
            out.println(curt);
        } else {
            out.println(-1);
        }

        out.close();
    }
}