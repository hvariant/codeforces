import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.StringTokenizer;

/**
 * Created by spss on 31/10/16.
 */
public class Problem260A {
    public static class MyScanner{
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner(){
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while(st == null || !st.hasMoreElements()){
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }
    }

    public static void main(String[] args) throws IOException{
        long a,b,n;
        MyScanner scan = new MyScanner();

        a = Long.parseLong(scan.next());
        b = Long.parseLong(scan.next());
        n = Long.parseLong(scan.next());

        int digit = 0;
        for(digit=0;digit<10;digit++)
            if((a * 10 + digit) % b == 0) break;

        if(digit == 10){
            System.out.println(-1);
        } else {
            System.out.print(a);
            System.out.print(digit);
            for(int i=0;i<n-1;i++) System.out.print(0);
            System.out.println();
        }
    }
}
