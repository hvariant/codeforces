/**
 * Created by spss on 16-12-6.
 */

import java.io.*;
import java.util.*;
import java.util.stream.IntStream;

public class Problem34A {
    private static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }


    private static class OutputWriter {
        private final PrintWriter writer;

        public OutputWriter(OutputStream outputStream) {
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
        }

        public OutputWriter(Writer writer) {
            this.writer = new PrintWriter(writer);
        }

        public void print(Object... objects) {
            for (int i = 0; i < objects.length; i++) {
                if (i != 0)
                    writer.print(' ');
                writer.print(objects[i]);
            }
        }

        public void println(Object... objects) {
            print(objects);
            writer.println();
        }

        public void close() {
            writer.close();
        }

        public void flush() {
            writer.flush();
        }

    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();
        OutputWriter out = new OutputWriter(System.out);

        int n = scan.nextInt();
        ArrayList<Integer> a = new ArrayList<>();
        for(int i=0;i<n;i++) a.add(scan.nextInt());

        ArrayList<Integer> diff = new ArrayList<>();
        for(int i=0;i<n-1;i++) diff.add(Math.abs(a.get(i+1) - a.get(i)));
        diff.add(Math.abs(a.get(a.size()-1) - a.get(0)));

        int minDiffIndex = IntStream.range(0,n).boxed().min((i1,i2) -> Integer.compare(diff.get(i1),diff.get(i2))).get();
        if(minDiffIndex == n-1){
            System.out.println(n + " 1");
        } else {
            System.out.println((minDiffIndex+1) + " " + (minDiffIndex+2));
        }

        out.close();
    }
}