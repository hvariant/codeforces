/**
 * Created by spss on 16-12-5.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.stream.IntStream;

public class Problem218B {
    public static class MyScanner {
        static BufferedReader br;
        static StringTokenizer st;

        MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
            st = null;
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreElements()) {
                st = new StringTokenizer(br.readLine());
            }

            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }

    public static void main(String[] args) throws IOException {
        MyScanner scan = new MyScanner();

        int n,m;
        ArrayList<Integer> A = new ArrayList<>();

        n = scan.nextInt();
        m = scan.nextInt();
        for(int i=0;i<m;i++) A.add(scan.nextInt());

        int maxVal = 0;
        ArrayList<Integer> a = new ArrayList<>(A);
        for(int _i=0;_i<n;_i++){
            final ArrayList<Integer> a_final = a;
            int i = IntStream.range(0,m)
                        .filter(k -> a_final.get(k) > 0)
                        .boxed()
                        .max((i1,i2) -> Integer.compare(a_final.get(i1),a_final.get(i2)))
                        .orElse(-1); //we shouldn't get to this step

            maxVal += a.get(i);
            a.set(i,a.get(i)-1);
        }

        int minVal = 0;
        a = new ArrayList<>(A);
        for(int _i=0;_i<n;_i++){
            final ArrayList<Integer> a_final = a;
            int i = IntStream.range(0,m)
                    .filter(k -> a_final.get(k) > 0)
                    .boxed()
                    .min((i1,i2) -> Integer.compare(a_final.get(i1),a_final.get(i2)))
                    .orElse(-1); //we shouldn't get to this step

            minVal += a.get(i);
            a.set(i,a.get(i)-1);
        }

        System.out.println(maxVal + " " + minVal);
    }
}